# simple-server

#### before
docker exec -t -i container_name /bin/bash

#### 1.0

docker build -t simplenodeserver:1.0 .

docker-compose up / docker-compose up -d

#### 2.0
env

#### 3.0
(.gitlab-ci.yml)
test
build
deploy

#### Doker Registry 

域名
certbot (certbot-auto)

```
docker run -d --restart=always --name registry -v `pwd`/certs:/certs -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/fullchain.pem -e REGISTRY_HTTP_TLS_KEY=/certs/privkey.pem  -p 5000:5000 registry:2
```
docker-compose.yml
<!-- registry:
  restart: always
  image: registry:2
  ports:
    - 5000:5000
  environment:
    REGISTRY_HTTP_TLS_CERTIFICATE: /certs/fullchain.pem
    REGISTRY_HTTP_TLS_KEY: /certs/privkey.pem
    REGISTRY_AUTH: htpasswd
    REGISTRY_AUTH_HTPASSWD_PATH: /auth/htpasswd
    REGISTRY_AUTH_HTPASSWD_REALM: Registry Realm
  volumes:
    - /home/registry:/var/lib/registry
    - /certs:/certs
    - /home/auth:/auth -->
registry:
  restart: always
  image: registry:2
  ports:
    - 5000:5000
  environment:
    REGISTRY_HTTP_TLS_CERTIFICATE: /letsencrypt/live/gitlab.atory.cc/fullchain.pem
    REGISTRY_HTTP_TLS_KEY: /letsencrypt/live/gitlab.atory.cc/privkey.pem
    REGISTRY_AUTH: htpasswd
    REGISTRY_AUTH_HTPASSWD_PATH: /auth/htpasswd
    REGISTRY_AUTH_HTPASSWD_REALM: Registry Realm
  volumes:
    - /home/registry:/var/lib/registry
    - /etc/letsencrypt:/letsencrypt
    - /home/auth:/auth

[https://docs.docker.com/registry/deploying/#native-basic-auth](https://docs.docker.com/registry/deploying/#native-basic-auth)

docker run \
  --entrypoint htpasswd \
  registry:2 -Bbn testuser testpassword > auth/htpasswd

docker pull distribution/registry:master
List all repositories (effectively images):
curl -X GET https://myregistry:5000/v2/_catalog
> {"repositories":["redis","ubuntu"]}
List all tags for a repository:
curl -X GET https://myregistry:5000/v2/ubuntu/tags/list
> {"name":"ubuntu","tags":["14.04"]}

https://docs.gitlab.com/ee/ci/docker/using_docker_build.html

#### 3.0
(.gitlab-ci.yml)
test  √
build √

#### 3.5
gitlab https

nginx['redirect_http_to_https'] = true
nginx['ssl_certificate'] = "/etc/letsencrypt/live/gitlab.atory.cc/fullchain.pem"
nginx['ssl_certificate_key'] = "/etc/letsencrypt/live/gitlab.atory.cc/privkey.pem"

#### 4.0 
配置 Registry [docker]

registry_external_url 'https://registry.atory.cc'

registry_nginx['ssl_certificate'] = "/etc/letsencrypt/live/registry.atory.cc/fullchain.pem"
registry_nginx['ssl_certificate_key'] = "/etc/letsencrypt/live/registry.atory.cc/privkey.pem"
### Settings used by GitLab application
# gitlab_rails['registry_enabled'] = true
# gitlab_rails['registry_host'] = "gitlab.atory.cc"
# gitlab_rails['registry_port'] = "5000"
# gitlab_rails['registry_path'] = "/var/opt/gitlab/gitlab-rails/shared/registry"
gitlab_rails['registry_path'] = "/home/registry"

使用Gitlab自有的模块，不能在外部启用 registry 否则会冲突

Registry启动后，可以使用 gitlab 账户密码登录

--
gitlab-runner register -n --url https://gitlab.atory.cc/ --registration-token HVT_iyskyyN9xWdEtVb2 --executor docker --description "node,docker,docker.sock" --docker-image "docker:latest" --docker-volumes /var/run/docker.sock:/var/run/docker.sock

gitlab-runner register -n --url https://gitlab.atory.cc/ --token HVT_iyskyyN9xWdEtVb2 --executor docker --description "node,docker,docker.sock" --docker-image "docker:latest" --docker-volumes /var/run/docker.sock:/var/run/docker.sock

* tls volumes ~/xx/docker:~/.docker

* registry 用户和用户组 

### Settings used by Registry application
# registry['enable'] = true
# registry['username'] = "registry"
# registry['group'] = "registry"

* chown -R registry:registry /home/registry


#### networked docker
https://docs.docker.com/engine/security/https/

In order to use the ingress network in the swarm, you need to have the following ports open between the swarm nodes before you enable swarm mode:

* Port 7946 TCP/UDP for container network discovery.
* Port 4789 UDP for the container ingress network.
