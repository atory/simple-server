const os = require('os');
const path = require('path');
const Koa = require('koa');
const config = require('config');
const winston = require('winston');
const APP_VERSION = require('./package').version;

const app = new Koa();

const HOST_NAME = os.hostname();
const SERVER_PORT = config.get('server.port');
const SERVER_ERR_LOG = config.get('server.errlog');
const SERVER_LOGS = config.get('server.logs');


const { createLogger, format, transports } = winston;
const { combine, timestamp, prettyPrint } = format;

const logger = createLogger({
  level: 'info',
  // format: winston.format.json(),
  format: combine(
    timestamp(),
    prettyPrint(),
  ),
  transports: [
    new transports.File({ filename: path.join(__dirname, SERVER_ERR_LOG), level: 'error' }),
    new transports.File({ filename: path.join(__dirname, SERVER_LOGS) }),
  ],
});
if (process.env.NODE_ENV === 'developer') {
  logger.add(new transports.Console({
    // format: winston.format.simple(),
  }));
}

app.use(async (ctx, next) => {
  const start = Date.now();
  ctx.logger = logger;
  try {
    await next();
  } catch (err) {
    logger.error(err);
  }
  const ms = Date.now() - start;
  ctx.set('X-Response-Time', `${ms}ms`);
});

app.use(async (ctx) => {
  ctx.body = `
    Hello World ${process.env.NODE_ENV}
    version: ${APP_VERSION}
    hostname: ${HOST_NAME}
  `;
});

app.listen(SERVER_PORT, () => {
  logger.info(`server start at ${SERVER_PORT} with version: ${APP_VERSION}`);
});
