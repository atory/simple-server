
const chai = require('chai');

const sumFunc = require('../../app/sum');

const { expect } = chai;

// let expected = '';
// let current = '';

before(() => {
  // expected = ['a', 'b', 'c'];
});

describe('String#split', () => {
  // beforeEach(() => {
  //   current = 'a,b,c'.split(',');
  // });

  // it('should return an array', () => {
  //   expect(Array.isArray(current)).to.be.equal(true);
  // });

  // it('should return the same array', () => {
  //   expect(expected.length).to.equal(current.length);
  //   for (let i = 0; i < expected.length; i += 1) {
  //     expect(expected[i]).equal(current[i]);
  //   }
  // });

  it('3 + 2 = 5', () => {
    expect(sumFunc(3, 2)).to.be.equal(5);
  });
});
